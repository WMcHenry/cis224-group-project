# README #

### What is this? ###

This is the number guessing game project. It allows the user 5 attempts to guess a number between 1 and 50.

### Author: Walker McHenry ###

Contact me at wmchenry608@ccsnh.edu.