var randomNum = Math.floor(Math.random() * 50) + 1;
var guessesLeft = 5;
var userGuess;
var message = document.getElementById("message");

console.log('script loaded successfully.');
console.log(randomNum);

window.onload = function() {
}

guess = function() {
    userGuess = document.getElementById("guessBox").value;
    
    if (guessesLeft <= 0) {
        message.innerHTML = "Sorry, you're out of guesses! Better luck next time."
    } else if (randomNum == userGuess) {
        message.innerHTML = "Congratulations! You guessed the right number.";
    } else if (randomNum < userGuess) {
        message.innerHTML = "Too high. You have " + guessesLeft + " more attempts.";
    } else if (randomNum > userGuess) {
        message.innerHTML = "Too low. You have " + guessesLeft + " more attempts.";
    }
    guessesLeft--;
}